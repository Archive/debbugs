#!/usr/bin/perl -wT

use strict;

chdir('/var/lib/debbugs/spool') || die "chdir spool: $!\n";

opendir(DIR,"db") || &quit("opendir db: $!\n");
my @list= grep(m/^\d+\.status$/,readdir(DIR));
grep(s/\.status$//,@list);
@list= sort { $a <=> $b } @list;

my %buglist;

my $f;
while (length($f=shift(@list))) {
    if (!open(S,"db/$f.status")) { next; }
    chop(my $s_originator= <S>);
    chop(my $s_date= <S>);
    chop(my $s_subject= <S>);
    chop(my $s_msgid= <S>);
    chop(my $s_package= <S>);
    chop(my $s_keywords= <S>);
    chop(my $s_done= <S>);
    chop(my $s_forwarded= <S>);
    $_= $s_package; y/A-Z/a-z/; $_= $` if m/[^-+._a-z0-9]/;
    my $package = $_;

    if (ref $buglist{$package}) {
      push @{$buglist{$package}}, $f;
    } else {
      $buglist{$package} = [$f];
    }
}

my $package;
foreach $package (keys %buglist) {
  my $count = $#{$buglist{$package}} + 1;
  print $package . ':' . $count . ': ' . join (' ', @{$buglist{$package}}) . "\n";
}
    
print "\n";

