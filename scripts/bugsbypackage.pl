#!/usr/bin/perl -w

use strict;

die "Usage: $0 thebuglist package" unless $#ARGV == 1;

my $filename = $ARGV[0];
my $package = $ARGV[1];

open BUGLIST, $filename or die "open ($filename): $!";
while (<BUGLIST>) {
    next unless $_ =~ $package;
    next unless s/^.*?://;
    print $_;
}
close BUGLIST;

