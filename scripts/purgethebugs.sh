#!/bin/bash

PACKAGE=$1
BUGS=`cat /var/tmp/martin/$PACKAGE.bugs`

for bug in $BUGS ; do mv -f /opt/debbugs/spool/db/$bug.* /xa/debbugs-moved/ ; done
